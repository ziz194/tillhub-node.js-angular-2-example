var express = require('express');
const cors = require('cors');
app = express(),
  port = process.env.PORT || 3000;
  mongoose = require('mongoose'),
  //Load the created model - task
  Product = require('./api/models/productModel'),
  bodyParser = require('body-parser');
  //Connect our database by adding a url to the mongoose instance connection and use bodyparser
  mongoose.Promise = global.Promise;
  mongoose.connect('mongodb://localhost/tillHubDb');
  const corsOptions = {
    origin: '*'
  }
  app.use(cors(corsOptions))
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  //Register routes to the server
  var routes = require('./api/routes/productRoutes');
  routes(app);
  app.listen(port);
  console.log('Tillhub example RESTful API server started on: ' + port);
