Made using Node.js for the API, Mongodb as databse and Angular 2 for the frontend. 

The app is made of two parts an API with node.js and an Angular 2 Client to consume the CRUD operations offered by the API

### Prerequisites

npm and mongodb have to be installed on your machine, if you don’t have them already, please go through these links:
* [npm installation guide](https://nodejs.org/en/download/package-manager)
* [mongodb installation guide](https://docs.mongodb.com/manual/installation)

### Quick start API

```bash

# install the dependencies with npm
$ npm install

### start mongodb server
$ mongod
OR on Windows with msi installer default settings 
$"C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe" --dbpath d:\link\to\mongodb\data\folder
###

# start the server
$ npm run start

```
Api is reachable through localhost:3000/products

* e.g localhost:3000/products/{{productID}} will return a  product with its id
* e.g localhost:3000/products/ will return a all products

### Quick start Frontend

```bash

# change directory to your app
$ cd frontend

# install the dependencies with npm
$ npm install

# start the server
$ npm start
```
go to [http://localhost:8080](http://localhost:8080) in your browser.
