import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation'
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductService } from './shared';
import { ProductSearchPipe } from './shared/pipes'
import { routing } from './app.routing';
import { ModalModule } from 'ng2-modal';
import { SimpleNotificationsModule } from 'angular2-notifications';
import {enableProdMode} from '@angular/core';

enableProdMode();


import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

@NgModule({
  imports: [
    BrowserModule,
    SimpleNotificationsModule.forRoot(),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
    CustomFormsModule,
    routing
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ProductSearchPipe
  ],
  providers: [
    ProductService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
