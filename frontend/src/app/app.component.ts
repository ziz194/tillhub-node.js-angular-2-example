import {Component, OnInit} from '@angular/core';

import '../style/app.scss';
declare var Slideout:any;

@Component({
  selector: 'my-app', // <my-app></my-app>
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{
  slideout:any;

  ngOnInit() {
    // initialise the slideout menu
    this.slideout = new Slideout({
      'panel': document.getElementById('panel'),
      'menu': document.getElementById('menu'),
      'padding': 256,
      'tolerance': 70,
      'easing': 'ease-in',
    });
  }

  constructor() {
  }
}
