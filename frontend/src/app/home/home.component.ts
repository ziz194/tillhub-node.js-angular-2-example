import { Component, OnInit } from '@angular/core';
import { ProductService } from '../shared';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'my-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  private products;
  private newProduct = {
      name : String ,
      descripction: String,
      price: Number,
      cuurency: String
  };
  slideout:any;

    constructor(
      private productService: ProductService,
      private notificationsService: NotificationsService
) {
    this.productService.getAllProducts().subscribe(data => this.products = data);
    this.newProduct.name = '';
  }
  ngOnInit() {
    console.log('Hello');
  }

  closeSlideOut(){
      $('#panel').css('transform','none');
      $('#panel').removeAttr('style');
  }

  addProduct() {
      this.productService.insertProduct(this.newProduct).then(
          result => {
              this.notificationsService.success('Product Added', result.message,{
                timeOut: 2000,
                showProgressBar: true,
            });
            this.productService.getAllProducts().subscribe(data => {
                this.products = data
            });
          }
      ).catch(
          error => {
            console.log('The product could not be added' + error);
          }
      );
  }

  deleteProduct(id){
    this.productService.deleteProduct(id).then(
    result => {
        this.notificationsService.error('Product Deleted', result.message,{
            timeOut: 2000,
            showProgressBar: true,
        });
        this.productService.getAllProducts().subscribe(data => this.products = data);
          $('#myModal').close();
    }
    ).catch(
        error => {
          console.log('The product could not be deleted' + error);
        }
    );
  }
}
