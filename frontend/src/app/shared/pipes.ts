import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'productFilter'
})
export class ProductSearchPipe implements PipeTransform {

    transform(value, search: string = '') {
        if (search != '') {
            return value.filter((item) => (
                item.description.toLowerCase().indexOf(search.toLowerCase()) != -1 ||
                item.name.toLowerCase().indexOf(search.toLowerCase()) != -1
            ));
        } else {
            return value;
        }
    }
}