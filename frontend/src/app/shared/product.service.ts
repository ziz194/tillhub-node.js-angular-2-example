import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers, RequestOptions } from '@angular/http';

let apiUrL = 'http://localhost:3000/products/';

@Injectable()
export class ProductService {
  constructor (
      private http: Http
  ) {}

  getAllProducts() {
    return this.http.get(apiUrL).map((res:Response) => res.json());
  }

  insertProduct(product) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    var body = 'name='+product.name+'&currency='+product.currency+'&description='+product.description+'&price='+product.price;
    return this.http.post(apiUrL, body, options).map(this.extractData).toPromise();
  }

  deleteProduct(id: number) {
    let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(apiUrL + id, options).map(this.extractData).toPromise();
  }
  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

}