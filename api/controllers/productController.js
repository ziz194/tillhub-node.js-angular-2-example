'use strict';


var mongoose = require('mongoose'),
Product = mongoose.model('products');

exports.list_all_products = function(req, res) {
  Product.find({}, function(err, product) {
    if (err)
        return res.send(err);
      return res.json(product);
  });
};


exports.create_a_product = function(req, res) {
  var new_product = new Product(req.body);
  new_product.save(function(err, product) {
    if (err)
      return res.send(err);
    return res.json({ message: 'Product has been added' });
  });
};


exports.read_a_product = function(req, res) {
  Product.findOne({ "_id" : req.params.productId }, function(err, product) {
    if (err){
        return res.send(err);
    }
    if(product === null){
        return res.json({ message: 'Product with id ' + req.params.productId +' not found'});
    } 
    else {
        return res.json(product);
    } 
  });
};


exports.update_a_product = function(req, res) {
  Product.findOneAndUpdate(req.params.productId, req.body, {new: true}, function(err, product) {
    if (err)
        return res.send(err);
      return res.json(product);
  });
};


exports.delete_a_product = function(req, res) {

  Product.remove({
    "_id" : req.params.productId
  }, function(err, product) {
    if (err)
        return res.send(err);
      return res.json({ message: 'Product successfully deleted' });
  });
};
