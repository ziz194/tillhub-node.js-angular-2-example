'use strict';
var mongoose = require('mongoose');
var createMongooseSchema = require('json-schema-to-mongoose/lib/json-schema');

var jsonSchema =
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "Product",
    "type": "object",
    "properties": {
        "id": {
            "type": "string"
        },
        "name": {
            "type": "string"
        },
        "description": {
            "type": "string",
                "maxLength": 540
        },
        "price": {
            "description": "Price in 'currency'",
                "type": "number",
                "minimum": 0
        },
        "currency": {
            "description": "ISO 4217 Currency Code",
                "type":"string",
                "minLength": 3,
                "maxLength": 3
        }
    },
    "required": ["id", "name", "description", "price", "currency"]
}

//Convert the schema
var schema = createMongooseSchema("http://json-schema.org/draft-04/schema#",jsonSchema);
var ProductSchema = new mongoose.Schema(schema)
module.exports = mongoose.model('products', ProductSchema);

